# Trip Builder
A Web Service API for Trips built using Laravel.

### Steps to Install

1. Edit the local hostname of the `bootstrap/start.php` environment.
2. Edit the settings within the files of the `app/config/local` directory.
3. Run the Migration: `php artisan migrate`
4. Run the Seeders: `php artisan db:seed`