<!DOCTYPE html>
<head>
    <title>Trip Builder</title>
    <style>
        div.wrapper   { margin: 0 auto; 
                        width: 960px; 
                        min-height: 600px;
                        border: 1px solid black;
                      }
        section.left  { float: left; width: 25%; border-right:1px solid black; }
        section.right { float: left; width: 70%; }
        section.main  { float: left; width: 100%; padding: 10px;}
        section       { min-height: 600px; padding: 10px; }
        table         { width: 100%; }
        td,th         { text-align: left; padding: 5px 0; }
    </style>
</head>
<body>
    <div class="wrapper">
        @yield('content')
    </div>
</body>
</html>