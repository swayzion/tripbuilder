@extends('layout')
@section('content')
<section class="main">
	<h1>My Trips</h1>
	<table>
		<th>id</th><th>name</th><th>created on</th>
		@foreach ($trips as $trip)
		<tr>
			<td>{{ $trip->id }}</td>
			<td>{{ link_to("trip/edit/$trip->id", $trip->name) }}</td>
			<td>{{ $trip->created_at }}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>{{ link_to("trip/create", "Create a new trip") }}</td>
			<td></td>
		</tr>
	</table>
</section>
@stop