@extends('layout')
@section('content')
<section class="main">
	<span style="color: green">{{ $message or '' }}</span>
	<table>
		{{ Form::open(array('url' => 'trip')) }}
		<tr>
			<td>
				{{ Form::label('name', 'Trip Name') }}
			</td>
			<td colspan="2">
				{{ Form::text('name') }}
				{{ Form::submit('Create') }}
			</td>
		</tr>	
		{{ Form::close() }}
		<tr>
			<td colspan="2">
				{{ link_to('/', 'Back to My Trips') }}
			</td>
		</tr>
	</table>
</section>
@stop