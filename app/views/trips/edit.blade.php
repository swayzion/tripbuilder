@extends('layout')
@section('content')
<section class="left">
	<h2>Add new flight</h2>
	{{ Form::open(array('url' => "flight")) }}
		From: <select name="from">
		@foreach ($trip->airports as $airport)
			<option value="{{$airport->id}}">{{$airport->name}}</option>
		@endforeach
		</select>
		<br/>
		To: <select name="to">
		@foreach ($trip->airports as $airport)
			<option value="{{$airport->id}}">{{$airport->name}}</option>
		@endforeach
		</select>
		<br/>
		{{ Form::hidden('trip', $trip->id )}}
		{{ Form::submit('Add') }}
	{{ Form::close() }}

	<p>{{ link_to('trip', 'View My Trips') }}</p>
</section>
<section class="right">
	<span style="color: green">{{ $message or '' }}</span>
	<table>
		{{ Form::open(array('url' => "trip/$trip->id", 'method' => 'patch')) }}
		<tr>
			<td>
				{{ Form::label('name', 'Trip Name') }}
			</td>
			<td colspan="2">
				{{ Form::text('name', $trip->name) }}
				{{ Form::submit('Rename') }}
			</td>
		</tr>	
		{{ Form::close() }}
	</table>

	<table>
		<th>From</th><th>To</th><th></th>
		@foreach($trip->flights as $flight)
			<tr>
				<td>{{ $flight->departure->name }} ({{ $flight->departure->code }})</td>
				<td>{{ $flight->arrival->name }} ({{ $flight->arrival->code }})</td>
				<td>
					{{ Form::open(array('url' => "flight/$flight->id", 'method' => 'delete')) }}
						{{ Form::hidden('trip', $trip->id )}}
						{{ Form::submit('Remove') }}
					{{ Form::close() }}
				</td>
			</tr>
		@endforeach
		<tr>
			<td colspan="3">You currently have {{ count($trip->flights) }} Flights in your trip.</td>
		</tr>
	</table>
</section>
@stop