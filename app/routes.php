<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require __DIR__ . '/domains/Trip/routes.php';
require __DIR__ . '/domains/Airport/routes.php';
require __DIR__ . '/domains/Flight/routes.php';

Route::get('/', function()
{
	return Redirect::action('Trip\\Apis\\TripApi@index');
});
