<?php

Route::group(array('prefix' => 'airport'), function () {

    Route::get('', [ 'uses' => 'Airport\\Apis\\AirportApi@index']);

});
