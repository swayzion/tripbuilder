<?php
namespace Airport\Entities;

class Airport extends \Eloquent
{
    protected $table = 'entity_airports';
    protected $guarded = ['id'];
}
