<?php
namespace Trip\Repositories;

use Trip\Entities\Trip;
use Airport\Entities\Airport;

class TripRepository
{

	/**
	 * Get all data required to edit a trip
	 * Requirement: Get the list of all airports (alphabetically)
	 *
	 * @param  int  $id
	 * @return Model
	 */
	public function withAirports($id)
	{
		$trip = Trip::find($id);

		$trip->airports = Airport::orderBy('name')->get();

		return $trip;
	}
}
