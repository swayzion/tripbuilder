<?php

Route::group(array('prefix' => 'trip'), function () {

    Route::get('', [ 'uses' => 'Trip\\Apis\\TripApi@index']);
    Route::get('create', [ 'uses' => 'Trip\\Apis\\TripApi@create']);
    Route::post('', [ 'uses' => 'Trip\\Apis\\TripApi@store']);
    Route::get('edit/{id}', [ 'uses' => 'Trip\\Apis\\TripApi@edit']);
    Route::delete('{id}', ['uses' => 'Trip\\Apis\\TripApi@destroy']);
    Route::patch('{id}', ['uses' => 'Trip\\Apis\\TripApi@update']);

});