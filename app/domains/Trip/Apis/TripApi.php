<?php

namespace Trip\Apis;

use \Input, \View, \Redirect;
use Trip\Repositories\TripRepository;
use Trip\Entities\Trip;

class TripApi extends \Support\BaseController 
{

	protected $repo;

	public function __construct(TripRepository $repo)
	{
		$this->repo = $repo;
	}

	/**
	 * Display a listing of Trips
	 *
	 * @return View
	 */
	public function index()
	{
		$trips = Trip::all();

		return View::make('trips.index', compact('trips'));
	}

	/**
	 * Show the form for creating a new trip
	 *
	 * @return View
	 */
	public function create()
	{
		return View::make('trips.create');
	}

	/**
	 * Store a newly created trip in storage.
	 *
	 * @return View
	 */
	public function store()
	{
		Trip::create(['name' => Input::get('name')]);

		return View::make('trips.create')->with('message', 'successfully added!');
	}

	/**
	 * Display the specified trip.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function show($id)
	{
		$trips = Trip::findOrFail($id);

		return View::make('trips.show', compact('trips'));
	}

	/**
	 * Show the form for editing the specified trips.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function edit($id)
	{
		$trip = $this->repo->withAirports($id);

		return View::make('trips.edit', compact('trip'));
	}

	/**
	 * Update the specified trips in storage.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function update($id)
	{
		$trip = Trip::findOrFail($id);

		$trip->update(['name' => Input::get('name')]);

		return Redirect::action('Trip\\Apis\\TripApi@edit', $id);
	}

}
