<?php
namespace Trip\Entities;

class Trip extends \Eloquent
{
    protected $table = 'entity_trips';
    protected $guarded = ['id'];

    public function flights()
	{
		return $this->hasMany('Flight\\Entities\\Flight', 'trip_id');
	}
}
