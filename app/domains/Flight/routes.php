<?php

Route::group(array('prefix' => 'flight'), function () {

    Route::get('', [ 'uses' => 'Flight\\Apis\\FlightApi@index']);
    Route::post('', [ 'uses' => 'Flight\\Apis\\FlightApi@store']);
    Route::delete('{id}', [ 'uses' => 'Flight\\Apis\\FlightApi@destroy']);

});
