<?php
namespace Flight\Entities;

class Flight extends \Eloquent
{
    protected $table = 'entity_flights';
    protected $guarded = ['id'];

    public function trip() {
    	return $this->belongsTo('Trip\\Entities\\Trip', 'trip_id');
	}

	public function departure() {
		return $this->belongsTo('Airport\\Entities\\Airport', 'from_airport');
	}

	public function arrival() {
		return $this->belongsTo('Airport\\Entities\\Airport', 'to_airport');
	}
}
