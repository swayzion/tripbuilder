<?php

namespace Flight\Apis;

use \Input, \Redirect;
use Flight\Entities\Flight;

class FlightApi extends \Support\BaseController 
{
	/**
	 * Store a newly created flight in storage.
	 *
	 * @return Redirect
	 */
	public function store()
	{	
		$params = Input::all();
		array_forget($params, '_token');
		
		Flight::create([
			'trip_id' => $params['trip'],
			'from_airport' => $params['from'],
			'to_airport' => $params['to']
		]);

		return Redirect::action('Trip\\Apis\\TripApi@edit', [$params['trip']]);
	}

	/**
	 * Remove the specified flight from storage.
	 *
	 * @return Redirect
	 */
	public function destroy($id)
	{
		Flight::destroy($id);

		return Redirect::action('Trip\\Apis\\TripApi@edit', [Input::get('trip')]);
	}

}
