<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntityFlights extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entity_flights', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('trip_id')->unsigned();
			$table->integer('to_airport')->unsigned();
			$table->integer('from_airport')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entity_flights');
	}

}
