<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('AirportsTableSeeder');
		$this->call('TripsTableSeeder');
		$this->call('FlightsTableSeeder');
	}

}
