<?php

class AirportsTableSeeder extends Seeder {

	public function run()
	{

		$data = [
            ['id' => 1, 'name' => 'Toronto Pearson International Airport', 'code' => 'YYZ'],
            ['id' => 2, 'name' => 'Billy Bishop Toronto City Airport', 'code' => 'YTZ'],
            ['id' => 3, 'name' => 'John F. Kennedy International Airport', 'code' => 'JFK'],
            ['id' => 4, 'name' => 'LaGuardia Airport', 'code' => 'LGA'],
            ['id' => 5, 'name' => 'Los Angeles International Airport', 'code' => 'LAX'],
            ['id' => 6, 'name' => 'Dallas/Forth Worth International Airport', 'code' => 'DFW'],
            ['id' => 7, 'name' => 'Miami International Airport', 'code' => 'MIA'],
            ['id' => 8, 'name' => 'Denver International Airport', 'code' => 'DEN'],
        ];

        foreach ($data as $airport) {
            \Airport\Entities\Airport::create($airport);
        }
	}

}