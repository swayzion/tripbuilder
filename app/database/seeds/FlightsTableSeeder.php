<?php

class FlightsTableSeeder extends Seeder {

	public function run()
	{
		\Flight\Entities\Flight::create([
			'trip_id' => 1,
			'to_airport' => 1,
			'from_airport' => 5
		]);
	}

}